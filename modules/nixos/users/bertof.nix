{
  users.users.bertof = {
    isNormalUser = true;
    extraGroups = [ "libvirtd" "kvm" "network" "wheel" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC3W3Btk1qtLHU69aFwseDuKU6PJMA+NxVXJXiRNhDce bertof@odin"
    ];
  };
}
