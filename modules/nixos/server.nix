{ lib, config, ... }:
let
  inherit (builtins) mapAttrs attrValues;
  inherit (lib) filterAttrs unique;
  btrfsFileSystems =
    filterAttrs (_k: v: v.fsType == "btrfs") config.fileSystems;
  btrfsDevices =
    unique (attrValues (mapAttrs (_: v: v.device) btrfsFileSystems));
in
{
  services = {
    btrfs.autoScrub = { enable = btrfsDevices != [ ]; fileSystems = btrfsDevices; };

    fstrim.enable = true;

    fwupd.enable = true;
  };

  nix.gc = { automatic = true; options = "--delete-older-than 7d"; };

  system.autoUpgrade = {
    enable = true;
    flake = "gitlab:bertof/nixos-machines";
    # dates = "daily"; # default 04:04
    randomizedDelaySec = "45min";
    flags = [ "--refresh" ];
    rebootWindow = {
      lower = "01:00";
      upper = "04:00";
    };
  };
}
