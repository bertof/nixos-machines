{
  security.sudo.wheelNeedsPassword = false;
  nix.settings = {
    trusted-users = [ "root" "@wheel" ];
    secret-key-files = [ "/etc/nix/key" ];
  };
}
