{ pkgs, ... }: {
  boot = {
    growPartition = true; # grow partition before boot
    loader = { systemd-boot.enable = true; efi.canTouchEfiVariables = true; };
    binfmt.emulatedSystems = [ "aarch64-linux" ]; # cross compile ARM 64
  };

  console = { font = "Lat2-Terminus16"; keyMap = "us"; };

  environment = {
    systemPackages = [ pkgs.neovim pkgs.helix pkgs.zellij pkgs.kitty.terminfo ];
  };

  networking = {
    hostName = "nixos-builder";
    firewall.enable = true;
  };

  time.timeZone = "Europe/Rome";

  services = {
    fail2ban = { enable = true; bantime-increment.enable = true; };
    openssh = { enable = true; openFirewall = true; };
    qemuGuest.enable = true;
  };

  system.stateVersion = "23.11";

  virtualisation.libvirtd.enable = true;
}
