{
  description = "Minimal flake environment";

  inputs = {
    dotfiles.url = "gitlab:bertof/nix-dotfiles";
    nixpkgs.follows = "dotfiles/nixpkgs";

    flake-parts.url = "github:hercules-ci/flake-parts";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
    systems.url = "github:nix-systems/default";
  };

  outputs = inputs@{ nixpkgs, ... }: inputs.flake-parts.lib.mkFlake { inherit inputs; } {
    systems = import inputs.systems;
    imports = [ inputs.pre-commit-hooks-nix.flakeModule ];
    perSystem = { config, pkgs, ... }: {
      # Per-system attributes can be defined here. The self' and inputs'
      # module parameters provide easy access to attributes of the same
      # system.

      # # This sets `pkgs` to a nixpkgs with allowUnfree option set.
      # _module.args.pkgs = import nixpkgs {
      #   inherit system;
      #   config.allowUnfree = true;
      # };

      pre-commit.settings.hooks = {
        deadnix.enable = true;
        nixpkgs-fmt.enable = true;
        statix.enable = true;
      };

      devShells.default = pkgs.mkShell {
        shellHook = ''
          ${config.pre-commit.installationScript}
        '';
      };

      formatter = pkgs.nixpkgs-fmt;
    };
    flake = {
      # The usual flake attributes can be defined here, including system-
      # agnostic ones like nixosModule and system-enumerating ones, although
      # those are more easily expressed in perSystem.

      nixosConfigurations =
        let
          commonModules = [
            {
              nix.registry.stable = { from = { id = "stable"; type = "indirect"; }; flake = nixpkgs; };
            }
            ./modules/nixos/nix.nix
            ./modules/nixos/builder.nix
            ./modules/nixos/users/bertof.nix
          ];
        in
        {
          nixos-builder = inputs.nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ [
              ./modules/nixos/server.nix

              ./instances/nixos-builder/hardware-configuration.nix
              ./instances/nixos-builder/configuration.nix


              # { home-manager.users.bertof = import ./instances/nixos-builder/hm/bertof.nix; }
            ];
          };
        };
    };
  };
}
